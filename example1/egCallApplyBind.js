// call
var obj = {num:2};
var obj2 = {num:6};

var addToThis = function(a, b, c) {
    return this.num + a + b + c;
};

console.log(addToThis.call(obj, 3, 5, 1));
console.log(obj);

// apply
var arr = [1, 2, 3];
console.log(addToThis.apply(obj, arr));
console.log(addToThis.apply(obj2, arr));

// bind
var arr = [1, 2, 3];
console.log(addToThis.bind(obj, arr)); // bind-uje f-ju i vraca je, ne izvrsava je.

var bound = addToThis.bind(obj);
console.dir(bound);

console.log(bound(1,2,3));