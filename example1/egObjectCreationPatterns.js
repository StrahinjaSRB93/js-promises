// Factory pattern
var peopleFactory = function(name, age, state) {
    var temp = {};

    temp.age = age;
    temp.name = name;
    temp. state = state;

    temp.printPerson = function() {
        console.log(this.name + ", " + this.age + ", " + this.state)
    };

    return temp;
};

// var person1 = peopleFactory("Mirta", 46, "YUG");
// var person2 = peopleFactory("Marijetica", 66, "HUN");

// person1.printPerson();
// person2.printPerson();

// Constructor pattern
var peopleConstructor = function(name, age, state) {
    this.name = name;
    this.age = age;
    this.state = state;

    this.printPerson = function() {
        console.log(this.name + ", " + this.age + ", " + this.state);
    };
};

// var person1 = new peopleConstructor("Mirta", 46, "YUG");
// var person2 = new peopleConstructor("Marijetica", 66, "HUN");

// person1.printPerson();
// person2.printPerson();

// Prototype pattern
var peopleProto = function() {

};
peopleProto.prototype.age = 0;
peopleProto.prototype.name = "no name";
peopleProto.prototype.city = "no city";

peopleProto.prototype.printPerson = function() {
    console.log(this.name + ", " + this.age + ", " + this.city);
};

// var person1 = new peopleProto();
// person1.name = 'Mirta';
// person1.age = 46;
// person1.city = 'NS';

// console.log('name' in person1);
// console.log(person1.hasOwnProperty('name'));

// person1.printPerson();
// console.dir(peopleProto);

// Dynamic prototype pattern

var peopleDynamicProto = function(name, age, state) {
    this.name = name;
    this.age = age;
    this.state = state;

    if(typeof this.printPerson !== 'function') {
        peopleDynamicProto.prototype.printPerson = function() {
            console.log(this.name + ", " + this.age + ", " + this.state);
        };
    }
};


var person1 = new peopleDynamicProto("Mirta", 46, "YUG");

person1.printPerson();