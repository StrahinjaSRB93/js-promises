let add = function(a, b) {
    return a + b;
};

let multiply = function(a, b) {
    return a * b;
};

let doWhatever = function(a, b) {
    console.log(`Here are your two numbers back ${a}, ${b}`);
};

let calc = function(num1, num2, callback) {
    if (typeof callback === "function") {
        return callback(num1, num2);
    }
};

console.log(calc(2, 3, multiply));

console.log(calc(7, 9, function(a, b) {
    return a - b;
}));

//---------------------------------------------------------------------

myArr = [{
    num: 5,
    str: 'apple'
}, {
    num: 7,
    str: 'cabbage'
}, {
    num: 1,
    str: 'ban'
}];

myArr.sort(function(val1, val2) {
    if (val1.num < val2.num) {
        return -1;
    } else {
        return 1;
    }
});

console.log(myArr);