let cleanRoom = function() {
	return new Promise(function(resolve, reject) { 
		resolve(' Cleaned The Room');
	});
};

let removeGarbage = function(message) {
	return new Promise(function(resolve, reject) { 
		resolve(message + ' Removed Garbage');
	});
};

let winIcecream = function(message) {
	return new Promise(function(resolve, reject) { 
		resolve(message + ' Won Icecream');
	});
};

// cleanRoom().then(function(result) { 
// 	return removeGarbage(result);
// }).then(function(result) {
// 	return winIcecream(result);
// }).then(function(result){
// 	console.log('finished' + result);
// });

Promise.all([cleanRoom(), removeGarbage(), winIcecream()]).then(
	function() {
		console.log('all finished');
	}
);

Promise.race([cleanRoom(), removeGarbage(), winIcecream()]).then(
	function() {
		console.log('one of them is finished');
	}
);